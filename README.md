# Linux learning reference

## Navigation

- Move directories
- List files
- Understand `.` and `..`
- Learn what "relative" and "absolute" paths are
- Understand `~`
- Find your current directory

## Interacting with files

- Make files
- Delete files
- Move files
- Copy files
- ... and all of the above for directories
- Find files
- Find files with certain text in them (`grep` - it's a hugely complicated command, so learn only the very basics to begin with)

## Interacting with text files

- Learn how to use a text editor like `nano` (`vim`/`vi` is what I use, but it's a whole new learning curve on its own)
- Then, without using a text editor, learn how to...
  - View the whole contents of a file in the terminal window (`cat`)
  - View large files with multiple "pages" (`less`)
  - View the first `x` number of lines of a file, and the last `x` lines (really useful for checking if a file has changed, or to make sure you're looking at the right file etc) (`head` and `tail`)
- Understand when/why you would use these commands

## Installing software

- Learn how to use a package manager such as `aptitude` in Ubuntu/Debian
  - Install, remove, list installed software
- Look around the net and see how some other software applications are installed in Linux - often using `git` and/or `curl`
- Understand linux "zipping" (`unzip`, `gunzip`, `tar`)

## System commands

- Find out which processes are running
- Find out how much CPU/RAM is being used
- Find out what logical and physical drives exist on the OS
- Find out how much space is left on the logical volumes
- Learn how to use the `history` command, especially with `grep`
- Learn how to use reverse search (`ctrl`+`shift`+`r`)

